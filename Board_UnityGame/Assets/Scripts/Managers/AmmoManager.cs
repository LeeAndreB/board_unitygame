﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoManager : MonoBehaviour
{
    //public int Ammo;
    // reference text component
    Text text;
    
    // Start is called before the first frame update
    void Awake()
    {
        text = GetComponent <Text> ();

        // resets score
        //Ammo = 60;
    }


    // updates score
    void Update()
    {
        GameObject GunBarrelEnd = GameObject.Find("GunBarrelEnd");
        PlayerShooting PlayerShooting = GunBarrelEnd.GetComponent<PlayerShooting>();
        //print("SCORE:" + score);
        text.text = "Ammo: " + PlayerShooting.Ammo;
    }
}

