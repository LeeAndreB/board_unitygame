﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    // public references to game componenets
    public PlayerHealth playerHealth;
    public GameObject enemy;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;


    void Start ()
    {
        InvokeRepeating ("Spawn", spawnTime, spawnTime);
    }


    void Spawn ()
    {
        // checks for player death
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }

        // sets spawn point
        int spawnPointIndex = Random.Range (0, spawnPoints.Length);

        // creats enemy at spawn point
        Instantiate (enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
}
