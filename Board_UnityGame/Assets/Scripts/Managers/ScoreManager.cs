﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;

    // reference text component
    Text text;


    // sets score at start of game
    void Awake ()
    {
        text = GetComponent <Text> ();

        // resets score
        score = 0;
    }


    // updates score
    void Update ()
    {
        //print("SCORE:" + score);
        text.text = "Score: " + score;
    }
}
