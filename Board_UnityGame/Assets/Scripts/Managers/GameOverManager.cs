﻿using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    // reference to player health
    public PlayerHealth playerHealth;


    Animator anim;


    void Awake()
    {
        // reference animator
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        // checks if player dead
        if (playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger("GameOver");
        }
    }
}
