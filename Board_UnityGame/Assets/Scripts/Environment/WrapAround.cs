﻿using UnityEngine;

public class WrapAround : MonoBehaviour
{
    public bool Left = false;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            if (Left)
            {
                other.gameObject.transform.position = new Vector3(other.gameObject.transform.position.z , other.gameObject.transform.position.y, other.gameObject.transform.position.x + 2);
            }
            else
            {
                other.gameObject.transform.position = new Vector3(other.gameObject.transform.position.z + 2, other.gameObject.transform.position.y, other.gameObject.transform.position.x);
                // other.gameObject.transform.SetPositionAndRotation(new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0));
                //other.gameObject.transform.position += transform.forward * 30f;
            }
            }
    }
}
