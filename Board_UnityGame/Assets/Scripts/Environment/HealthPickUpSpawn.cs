﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUpSpawn : MonoBehaviour
{
    public GameObject[] pickupPrefabs;
    GameObject spawnedPickup;
    public float waitToSpawn = 10f;

    public Quaternion position { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        if (spawnedPickup == null)
        {
            if (waitToSpawn <= 0f)
                spawnPickup();
            else
                waitToSpawn -= Time.deltaTime;
        }
    }

    void spawnPickup()
    {
        GameObject toSpawn = pickupPrefabs[(int)(Random.value * pickupPrefabs.Length)];

        spawnedPickup = Instantiate(toSpawn, transform.position, toSpawn.transform.rotation) as GameObject;

        waitToSpawn = Random.Range(20f, 40f);
    }
    /*void Update()
    {
        if (waitToSpawn >= 1)
        { 
            waitToSpawn -= Time.deltaTime;
        }
        else 
        {
            spawnPickup();
        }
       

        if (spawnedPickup == null)
        {
            if (waitToSpawn <= 0f)
            {
                spawnPickup();
            }
            else
            {
                waitToSpawn -= Time.deltaTime;
            }
        }
        void spawnPickup()
        {
            GameObject pickupToSpawn = pickupPrefabs[(int)(Random.value * pickupPrefabs.Length)];
            spawnedPickup = Instantiate(pickupToSpawn, transform.position, pickupToSpawn.transform.rotation) as GameObject;
            waitToSpawn = Random.Range(10f, 30f);
        }*/
}