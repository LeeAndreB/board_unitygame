﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    // how often enemy attacks
    public float timeBetweenAttacks = 0.5f;

    // amount of damage
    public int attackDamage = 10;


    // stores animator component
    Animator anim;

    // referenncve to player
    GameObject player;

    // reference to player health script
    PlayerHealth playerHealth;

    // reference to enemy health script
    EnemyHealth enemyHealth;

    // checks if player is close enough
    bool playerInRange;

    // keeps everything in siync
    float timer;


    void Awake ()
    {
        // locates player
        player = GameObject.FindGameObjectWithTag ("Player");

        // stores reference to player
        playerHealth = player.GetComponent <PlayerHealth> ();


        enemyHealth = GetComponent <EnemyHealth>();

        // reference animator component
        anim = GetComponent <Animator> ();
    }


    void OnTriggerEnter (Collider other)
    {
        // checks for player
        if (other.gameObject == player)
        {
            // says player is close enough
            playerInRange = true;
        }
    }


    void OnTriggerExit (Collider other)
    {
        // checks for player
        if (other.gameObject == player)
        {
            // says player is no longer close enough
            playerInRange = false;
        }
    }


    void Update ()
    {
        // how much time has occured
        timer += Time.deltaTime;

        // if enough time has passed and player is in range attack
        if (timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0)
        {
            Attack ();
        }

        // checks for player death
        if (playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger ("PlayerDead");
        }
    }


    void Attack ()
    {
        // resets timer evry attack
        timer = 0f;

        // makes sure player still has health to lose
        if (playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage (attackDamage);
        }
    }
}
