﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    // what enemy moves toward
    Transform player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;

    // reference ot nav mesh agent
    UnityEngine.AI.NavMeshAgent nav;


    void Awake ()
    {
        // finds object with player tag
        player = GameObject.FindGameObjectWithTag ("Player").transform;
        playerHealth = player.GetComponent <PlayerHealth> ();
        enemyHealth = GetComponent <EnemyHealth> ();

        // references component in editor
        nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
    }


    void Update ()
    {
        if(enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
        // tells nav mesh agent to walk towards player
        nav.SetDestination (player.position);
        }
        else
        {
        nav.enabled = false;
        }
    }
}
