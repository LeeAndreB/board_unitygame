﻿using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    // sets start health
    public int startingHealth = 100;

    // stores current health
    public int currentHealth;

    // how fast enemies sink through the floor after death
    public float sinkSpeed = 2.5f;

    // change in score on death
    public int scoreValue = 10;

    // calls on audio file
    public AudioClip deathClip;


    // private variables
    Animator anim;
    AudioSource enemyAudio;
    ParticleSystem hitParticles;
    CapsuleCollider capsuleCollider;
    bool isDead;
    bool isSinking;


    void Awake ()
    {
       // reference to game components
        anim = GetComponent <Animator> ();
        enemyAudio = GetComponent <AudioSource> ();
        hitParticles = GetComponentInChildren <ParticleSystem> ();
        capsuleCollider = GetComponent <CapsuleCollider> ();

        // set current health to start health
        currentHealth = startingHealth;
    }


    void Update ()
    {
        // is enemy sinking
        if (isSinking)
        {
            // change in position when sinking
            transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
        }
    }


    public void TakeDamage (int amount, Vector3 hitPoint)
    {
        // ignore function if enemy is dead
        if (isDead)
            return;

        enemyAudio.Play ();

        // chaneg in health
        currentHealth -= amount;
        
        // move hit particles to where hit
        hitParticles.transform.position = hitPoint;
        hitParticles.Play();

        if(currentHealth <= 0)
        {
            // starts death function
            Death ();
        }
    }


    void Death ()
    {
        // marks enemy as dead
        isDead = true;

        // lets player move through them
        capsuleCollider.isTrigger = true;

        anim.SetTrigger ("Dead");

        // starts death animation and audiio
        enemyAudio.clip = deathClip;
        enemyAudio.Play ();
    }


    public void StartSinking ()
    {
        // stops AI
        GetComponent <UnityEngine.AI.NavMeshAgent> ().enabled = false;
        GetComponent <Rigidbody> ().isKinematic = true;
        isSinking = true;
        ScoreManager.score += scoreValue;

        // gets rid of enemy after 2 seconds
        Destroy (gameObject, 2f);
    }
}
