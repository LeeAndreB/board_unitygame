﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    // damage done
    public int damagePerShot = 25;

    // how quickly gun can fire
    public float timeBetweenBullets = 0.15f;

    // range of bullet
    public float range = 100f;


    // keeps all timers in sync
    float timer;

    // raycast to find point 
    Ray shootRay = new Ray();

    // stores what is hit
    RaycastHit shootHit;

    // shootable mask
    int shootableMask;

    // reference to components
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;

    // how long effects are viewable
    float effectsDisplayTime = 0.2f;

    // ammo count
    public float Ammo = 100;

    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();

        // happens when shooting
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
    }


    void Update ()
    {
        // is it time to shoot
        timer += Time.deltaTime;

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && Ammo >= 1)
        {
            Shoot ();
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }
    }


    public void DisableEffects ()
    {
        // when light and such from gun shot is visible
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot ()
    {
        // resets the time between shots
        timer = 0f;

        // triggers shoot audio and animations
        gunAudio.Play ();

        // change in ammo
        Ammo -= 1;
        //print(Ammo);

        gunLight.enabled = true;

        // resets particles
        gunParticles.Stop ();
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        // checks if anything eligible is hit
        if (Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }
    /*public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup_Ammo"))
        {
            Ammo += 10;
        }
    }*/

}
