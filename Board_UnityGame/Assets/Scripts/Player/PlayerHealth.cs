﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
    // health at begining
    public float startingHealth = 100;

    // health at any point
    public float currentHealth;

    // reference to slider game element
    public Slider healthSlider;

    // reference to damage image
    public Image damageImage;

    // reference to death audio
    public AudioClip deathClip;

    // how quickly damage image shows on screen
    public float flashSpeed = 5f;

    public float healthGen = 60f;

    // sets color of flash
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);


    //REFERNCES ANIMATOR COMPONENT
    Animator anim;

    // references audio
    AudioSource playerAudio;

    // references script
    PlayerMovement playerMovement;
    
    
    PlayerShooting playerShooting;

    // is player dead
    bool isDead;

    // has player taken damage
    bool damaged;


    void Awake ()
    {
        //references components
        anim = GetComponent <Animator> ();
        playerAudio = GetComponent <AudioSource> ();
        playerMovement = GetComponent <PlayerMovement> ();
        playerShooting = GetComponentInChildren <PlayerShooting> ();
        currentHealth = startingHealth;
        healthGen = 60f;
    }


    void Update ()
    {
        // after taken damage
        if (damaged)
        {
            // shows damage image
            damageImage.color = flashColour;
        }
        else
        {
            // fades damge image
            damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);

            // health regeneration
            healthRegen();
        }
        damaged = false;
        healthGen -= 1;
    }


    public void TakeDamage (int amount)
    {
        damaged = true;

        // updates health
        currentHealth -= amount;

        // updates health slider
        healthSlider.value = currentHealth;

        // plays hurt audio
        playerAudio.Play ();

        healthGen = 60f;
        print("Health regen reset");

        // death function
        if (currentHealth <= 0 && !isDead)
        {
            Death ();
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup_Health"))
        {
            currentHealth += 10;
            healthSlider.value = currentHealth;
        }
    }

    void Death ()
    {
        // sets death 
        isDead = true;

        playerShooting.DisableEffects ();

        // plays death animation and audio
        anim.SetTrigger ("Die");

        playerAudio.clip = deathClip;
        playerAudio.Play ();

        // stops player from moving if deaad
        playerMovement.enabled = false;
        playerShooting.enabled = false;
    }


    public void RestartLevel ()
    {
        // restarts ganme
        SceneManager.LoadScene (0);
    }

    public void healthRegen()
    {
        //print("HealthRegen!");
        if (currentHealth >= 1 && !isDead && healthGen <= 0 && currentHealth <= 99)
        {
            currentHealth += 1;
            print(currentHealth);
        }
    }

}
