﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // player's speed
    public float speed = 6f;

    // private variables
    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100f;

    void Awake()
    {
        // layer mask for the floor layer
        floorMask = LayerMask.GetMask("Floor");

        // references
        anim = GetComponent <Animator> ();
        playerRigidbody = GetComponent<Rigidbody> ();
    }

    private void FixedUpdate()
    {
        // input axes storage
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        // moves player
        Move(h, v);

        // Turn the player to face the mouse
        Turning();

        // animation for player
        Animating(h, v);
    }

    void Move (float h, float v)
    {
        // sets movement
        movement.Set(h, 0f, v);

        // normalizes movement
        movement = movement.normalized * speed * Time.deltaTime;

        // moves player to position plus movement
        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Turning()
    {
        // makes ray from mouse to camera
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // makes variable to store what ray hit
        RaycastHit floorHit;

        // performs raycast and sets up if hit floor layer
        if(Physics.Raycast (camRay, out floorHit, camRayLength, floorMask))
        {
            // vector from player to floor where mouse hit
            Vector3 playerToMouse = floorHit.point - transform.position;
            
            // aligns vector with floor plane
            playerToMouse.y = 0f;

            // makes quaternion based on vector from mouse to player
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            // set player rotation to new rotation
            playerRigidbody.MoveRotation(newRotation);
        }
    }

    void Animating(float h, float v)
    {
        // makes boolean that is true if either input axis isn't 0
        bool walking = h != 0f || v != 0f;

        //b tell the animator if player is walking
        anim.SetBool("IsWalking", walking);
    }
}
