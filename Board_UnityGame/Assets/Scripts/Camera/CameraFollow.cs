﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // position of the camera
    public Transform target;

    // the speed of the camera
    public float smoothing = 5f;

    // initial offset from target
    Vector3 offset;

    private void Start()
    {
        // finds initial offset
        offset = transform.position - target.position;
    }

    private void FixedUpdate()
    {
        // finds target position
        Vector3 targetCamPos = target.position + offset;

        // smoothly moves camera
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    }
}
